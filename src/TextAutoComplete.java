import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;

public class TextAutoComplete extends JFrame implements KeyListener{
	
	private static final long serialVersionUID = 1L;
	JPanel panel = new JPanel();
	JTextField textField = new JTextField(20);
	JTextArea textArea = new JTextArea(1,1);
	static Trie newTrie;
	String currentString = "";
	
	public static void main(String args[]) {
		newTrie = new Trie("C:/Users/brian_000/workspace/AutoComplete/src");
		new TextAutoComplete();
		
	}
	
	public TextAutoComplete(){
		super("Type something!");
		
		setSize(250, 100);
		setResizable(true);
		
		panel.add(textField);
		add(panel);
		textField.addKeyListener(this);
		setVisible(true);
	}

	@Override
	public void keyTyped(KeyEvent e) {
		// TODO Auto-generated method stub
	
	}

	@Override
	public void keyPressed(KeyEvent e) {
		// TODO Auto-generated method stub
		if(e.getKeyCode() == 8){
			if(currentString.length() > 0){
				currentString = currentString.substring(0, currentString.length()-1);
				if(currentString.length() > 0){
					newTrie.recursiveTraversal(newTrie.root ,currentString);
					System.out.println();
				}
			}
		}
		if(e.getKeyCode() >= 65 && e.getKeyCode() <=90){
				currentString += Character.toString(e.getKeyChar());
			if(currentString.length() > 0){	
				newTrie.recursiveTraversal(newTrie.root ,currentString);
				System.out.println();
			}
		}
	}

	@Override
	public void keyReleased(KeyEvent e) {
		// TODO Auto-generated method stub
		
	}

	
}
