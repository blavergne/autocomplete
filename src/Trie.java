import java.io.BufferedReader;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.*;

import javax.swing.JFrame;

public class Trie{
	Node root = null;
	
	public Trie(String stringPath){
		Charset charset = Charset.forName("US-ASCII");
		Path newPath = Paths.get(stringPath, "dictionary2.txt");
		
		try(BufferedReader reader = Files.newBufferedReader(newPath, charset)){
			String line = null;
			while((line = reader.readLine()) != null){
				if(root == null){
					root = new Node(line);
				}
				else{
					Node currentNode = root;
					Node parentNode = null;
					int index = 0;
					
					while(true){
						if(index == line.length()){
							currentNode.setIsWord(true);
							break;
						}
						if(currentNode.arrayNodeNotNull(line.charAt(index)) && index < line.length()){
							currentNode = currentNode.nextNode(line.charAt(index));
							index++;
						}
						else{
							parentNode = currentNode;
							currentNode.letterArray[letterToIndex(line.charAt(index))] = new Node(line.substring(0, index+1));
							currentNode = currentNode.nextNode(line.charAt(index));
							currentNode.parent = parentNode;
							index++;
						}
					}
				}
			}
		}catch (IOException x){
			System.err.format("IOException: %s%n", x);
		}	
	}
	
	//recursively traverses trie and prints to console all real words
	public void recursiveTraversal(Node trieNode, String string){
		 Node currentNode = trieNode;
		
		 if(trieNode == root){
			 int stringIndex = 0;
			 //currentNode = currentNode.letterArray[letterToIndex(string.charAt(0))];
			 while(stringIndex < string.length()){
				 if(currentNode != null){
					 currentNode = currentNode.letterArray[letterToIndex(string.charAt(stringIndex))];
					 stringIndex++;
				 }
				 else{
					 return;
				 }
			 }
		 }
		 
		 if(currentNode != null){
			 if(currentNode.isWord){
				 if(string.length() == 1){
					 if(currentNode.getWordString().length() <= 3){
						 System.out.println(currentNode.getWordString());
					 }
				 }
				 else{
					 if(currentNode.getWordString().length() > 2){
						 System.out.println(currentNode.getWordString());
					 }
				 }
			 }
		 }
		 else{
			 return;
		 }
		 
		 for(int i=0; i < 26; i++){
			 if(currentNode.letterArray[i] != null){
				recursiveTraversal(currentNode.letterArray[i], string);
			 }
		 }
		 
		 return;
		 
	}
	
	public int letterToIndex(char letter){
		int index = 0;
				
			switch(letter){
				case 'a': index = 0; break;
				case 'b': index = 1; break;	
				case 'c': index = 2; break;
				case 'd': index = 3; break;
				case 'e': index = 4; break;
				case 'f': index = 5; break;
				case 'g': index = 6; break; 
				case 'h': index = 7; break;
				case 'i': index = 8; break;
				case 'j': index = 9; break;
				case 'k': index = 10; break;
				case 'l': index = 11; break;
				case 'm': index = 12; break;
				case 'n': index = 13; break;
				case 'o': index = 14; break;
				case 'p': index = 15; break;
				case 'q': index = 16; break;
				case 'r': index = 17; break;
				case 's': index = 18; break;
				case 't': index = 19; break;
				case 'u': index = 20; break;
				case 'v': index = 21; break;
				case 'w': index = 22; break;
				case 'x': index = 23; break;
				case 'y': index = 24; break;
				case 'z': index = 25; break;
			}
			
		return index;
				
	}

}


