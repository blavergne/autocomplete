

public class Node {
	private String wordString;
	private int wordStringLength;
	Node parent;
	Node letterArray[];
	boolean isWord = false;
	
	public Node(String wordString){
		letterArray = new Node[26];
		this.wordString = wordString;
		wordStringLength = wordString.length();
		for(int i = 0; i < 26; i++){
			letterArray[i] = null;
		}
	}
	
	public void setWordString(String val){
		wordString = val;
	}
	
	public String getWordString(){
		return  wordString;
	}
	
	public int getWordStringLength(){
		return wordStringLength;
	}
	
	public void setIsWord(boolean val){
		isWord = val;
	}
	
	public boolean getIsWord(){
		return isWord;
	}
	
	public Node nextNode(char letter){
		int index = 0;
				
			switch(letter){
				case 'a': index = 0; break;
				case 'b': index = 1; break;	
				case 'c': index = 2; break;
				case 'd': index = 3; break;
				case 'e': index = 4; break;
				case 'f': index = 5; break;
				case 'g': index = 6; break; 
				case 'h': index = 7; break;
				case 'i': index = 8; break;
				case 'j': index = 9; break;
				case 'k': index = 10; break;
				case 'l': index = 11; break;
				case 'm': index = 12; break;
				case 'n': index = 13; break;
				case 'o': index = 14; break;
				case 'p': index = 15; break;
				case 'q': index = 16; break;
				case 'r': index = 17; break;
				case 's': index = 18; break;
				case 't': index = 19; break;
				case 'u': index = 20; break;
				case 'v': index = 21; break;
				case 'w': index = 22; break;
				case 'x': index = 23; break;
				case 'y': index = 24; break;
				case 'z': index = 25; break;
			}
			
		return letterArray[index];
				
	}
	
	public boolean arrayNodeNotNull(char letter){
		
		int index = 0;
		
		switch(letter){
			case 'a': index = 0; break;
			case 'b': index = 1; break;	
			case 'c': index = 2; break;
			case 'd': index = 3; break;
			case 'e': index = 4; break;
			case 'f': index = 5; break;
			case 'g': index = 6; break; 
			case 'h': index = 7; break;
			case 'i': index = 8; break;
			case 'j': index = 9; break;
			case 'k': index = 10; break;
			case 'l': index = 11; break;
			case 'm': index = 12; break;
			case 'n': index = 13; break;
			case 'o': index = 14; break;
			case 'p': index = 15; break;
			case 'q': index = 16; break;
			case 'r': index = 17; break;
			case 's': index = 18; break;
			case 't': index = 19; break;
			case 'u': index = 20; break;
			case 'v': index = 21; break;
			case 'w': index = 22; break;
			case 'x': index = 23; break;
			case 'y': index = 24; break;
			case 'z': index = 25; break;
		}
		
		if(letterArray[index] != null){
			return true;
		}
		
		return false;
		
	}
	
}
